<?php
namespace App\View\Helper;

use Cake\View\Helper;

class BasicHelper extends Helper
{

    public function shortTitle($title, $length)
    {
        $msgTrimmed = mb_substr($title,0,$length);
		$lastSpace = strrpos($msgTrimmed, ' ', 0);
		if(strlen($title)>100) {
			$string = mb_substr($msgTrimmed,0,$lastSpace).'...';	
		} else {
			$string = mb_substr($msgTrimmed,0,$lastSpace);
		}
		return $string;
    }
}
?>
<?php
namespace App\Utility;

use App\Controller\Admin\SellingCoachReportsController;
/**
* 
*/
class Pdf extends \FPDF
{       
        public $header_title;
        public $left_name;
	
	function Header()
	{

        date_default_timezone_set('America/Los_Angeles');
        
        $sellobj = new SellingCoachReportsController();
        $admin_user = $sellobj->getadminuser();
        //$header_title = $sellobj->getheadertitle();

        $this->SetFont('Arial','',10);
        $this->Cell(20,5,'',0,1);
        $this->SetXY(35,$this->GetY()-5);
        $this->Cell(20,5,'',0,1);

        $this->SetXY(10,$this->GetY());
        $this->Cell(20,5,$this->left_name,0,1);
        $this->SetXY(34,$this->GetY()-5);
        $this->Cell(20,5,ucfirst($admin_user),0,1);


        $this->SetFont('Arial','B',25);
        $this->SetXY(72,$this->GetY()-10);
        $this->Cell(20,10,$this->header_title,0,1);

        $this->SetFont('Arial','',10);
        $this->SetXY(155,$this->GetY()-10);
        $this->Cell(20,5,'Printed Date',0,1);
        $this->SetXY(180,$this->GetY()-5);
        $date = date('m/d/Y');
        $this->Cell(200,5,$date,0,1);

        $this->SetXY(155,$this->GetY());
        $this->Cell(20,5,'Time',0,1);
        $this->SetXY(180,$this->GetY()-5);
        $time = date("g:i a", time()-8);
        $this->Cell(20,5,$time,0,1);
        
	    // Line break
	    $this->Ln(20);
	}

	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}



}

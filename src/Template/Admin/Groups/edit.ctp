<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create() ?>
  <div class="">
    <div class="card-body">
      <?= $this->Form->create($group) ?>
        <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">Group Name</label>
            <div class="col-10">
                <?= $this->Form->control('name',['label' =>false,'class' => 'form-control','placeholder' => 'Enter Group Name']) ?>
            </div>
        </div>


        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label"></label>
            <div class="col-10">
                <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
            </div>
        </div>
      </form>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>

<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Rokhardware-Dashboard';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $cakeDescription ?>
        <?php echo $this->fetch('title') ?>
    </title>
    <?php echo $this->Html->meta('icon') ?>
    <?php echo $this->element('cssjs');?>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <div class="overlay" style="display: none;">
      <img src="<?php echo $this->Url->image('loading_icon.gif');?>" width="50">
    </div>
    <?php echo $this->element('header');?>
<?php echo $this->Html->script('vendor/jquery/jquery.min.js'); ?>

    <div class="content-wrapper">

      <div class="container-fluid">
      <?= $this->Flash->render() ?>

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item active"><a href=""><?= $this->fetch('title') ?></a></li>
        </ol>

        <?= $this->fetch('content') ?>
        
      </div>
      <!-- /.container-fluid -->

    </div>
    <?php echo $this->element('footer');?>
</body>
</html>

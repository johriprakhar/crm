<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create() ?>
  <div class="">
    <div class="card-body">
      <?= $this->Form->create($user) ?>
        <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">User Name</label>
            <div class="col-10">
                <?= $this->Form->control('username',['label' =>false,'class' => 'form-control','placeholder' => 'Enter username']) ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Password</label>
            <div class="col-10">
                <?= $this->Form->control('password',['label' =>false,'type' => 'password','class' => 'form-control','placeholder' => 'Password']) ?>   
            </div>
        </div>
        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Email</label>
            <div class="col-10">
                <?= $this->Form->control('email',['label' =>false,'type' => 'email','class' => 'form-control ','placeholder' => 'Email']) ?>
            </div>
        </div>
		<div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Group</label>
            <div class="col-10">
                <?php 
				 $request = $this->Url->build(['action' => 'ajaxRoleByGroup', 'ext' => 'json']);
				echo $this->Form->control('group_id',['id' => 'groupField', 'rel' => $request,'class'=>'form-control required','options' => $groups,'label'=>false,'empty'=>'Select Group']);?>
            </div>
        </div>
		  <div class="form-group row">
			<label for="example-text-input" class="col-2 col-form-label">Role</label>
			<div class="col-10">
				<?php echo $this->Form->control('role_id',['id' => 'roleField','class'=>'form-control required','options' => $roles,'label'=>false]);?>
			</div>
		</div>
       
        
        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label"></label>
            <div class="col-10">
                <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
            </div>
        </div>
      </form>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>
<script>
$(document).ready(function() {
    $('#groupField').change(function() 
    {
        var targeturl = $(this).attr('rel');
        var group_id = $(this).val();
        $.ajax({
            type: 'get',
            url: targeturl + '&id=' + group_id,
            beforeSend: function(xhr) 
            {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) 
            {
                if (response.data) 
                {
                    $('#roleField').html(response.data.roles);
                }
            },
            error: function(e) 
            {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    });
});
</script>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <a class="navbar-brand" href="<?php echo $this->Url->build(["prefix"=>"admin",
                "controller" => "Users",
                "action" => "index",
				"plugin" => Null
            ]);?>">Rokhardware</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
          <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="<?php echo $this->Url->build(["prefix"=>"admin",
                "controller" => "Users",
                "action" => "index",
				"plugin" => Null
            ]);?>">
              <i class="fa fa-fw fa-dashboard"></i>
              <span class="nav-link-text">
                Dashboard</span>
            </a>
          </li>
          
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Users">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span class="nav-link-text">
                Users</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseComponents">
              <li>
                <?= $this->Html->link(__('Add New User'), ['controller'=>'users','action' => 'add',"plugin" => Null]) ?>
              </li>
              <li>
                <?= $this->Html->link(__('User Listing'), ['prefix'=>'admin','controller'=>'users','action' => 'listing',"plugin" => Null]) ?>
              </li>
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reports">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#report" data-parent="#exampleAccordion">
              <i class="fa fa-table" aria-hidden="true"></i>
              <span class="nav-link-text">
                Reports</span>
            </a>
            <ul class="sidenav-second-level collapse" id="report">
              <li>
                <?= $this->Html->link(__('Upload Selling Coach'), ['controller'=>'SellingCoachReports','action' => 'uploadReport',"plugin" => Null]) ?>
              </li>
              <li>
                <?= $this->Html->link(__('Fetch SkuVault Inventory'), ['controller'=>'SkuVaultReports','action' => 'add',"plugin" => Null]) ?>
              </li>
              <li>
                <?= $this->Html->link(__('Fba Report'), ['controller'=>'SellingCoachReports','action' => 'report',"plugin" => Null]) ?>
              </li>
              <li>
                <?= $this->Html->link(__('FBA Pick List'), ['controller'=>'SellingCoachReports','action' => 'kits',"plugin" => Null]) ?>
              </li>
              <li>
                <?= $this->Html->link(__('Restock List'), ['controller'=>'SellingCoachReports','action' => 'restock',"plugin" => Null]) ?>
              </li>
              <li>
                <?= $this->Html->link(__('Work Flow'), ['controller'=>'SellingCoachReports','action' => 'workFlow',"plugin" => Null]) ?>
              </li>
              <!--<li>
                <?= $this->Html->link(__('Selling Coach Report'), ['controller'=>'SellingCoachReports','action' => 'index',"plugin" => Null]) ?>
              </li>
               <li>
                <?= $this->Html->link(__('Fba Report'), ['controller'=>'SellingCoachReports','action' => 'fba-reports',"plugin" => Null]) ?>
              </li>
              
              
              <li>
                <?= $this->Html->link(__('SkuVault Data'), ['controller'=>'SkuVaultReports','action' => 'index',"plugin" => Null]) ?>
              </li>-->
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Overstock">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#overstock" data-parent="#exampleAccordion">
              <i class="fa fa-table" aria-hidden="true"></i>
              <span class="nav-link-text">
                Overstock</span>
            </a>
            <ul class="sidenav-second-level collapse" id="overstock">
              <li>
                <?= $this->Html->link(__('Orders'), ['controller'=>'Orders','action' => 'index',"plugin" => Null]) ?>
              </li>
              
            </ul>
          </li>
		            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="acl">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#acl" data-parent="#exampleAccordion">
              <i class="fa fa-table" aria-hidden="true"></i>
              <span class="nav-link-text">
                ACL</span>
            </a>
            <ul class="sidenav-second-level collapse" id="acl">
				<li>
			
				<?= $this->Html->link(__('Add New Group'), ['prefix'=>'admin','controller'=>'groups','action' => 'add',"plugin" => Null]) ?>
              </li>
			  <li>
			
				<?= $this->Html->link(__('Group Listing'), ['prefix'=>'admin','controller'=>'groups','action' => 'listing',"plugin" => Null]) ?>
              </li>
			  <li>
			
				<?= $this->Html->link(__('Add New Role'), ['controller'=>'roles','action' => 'add',"plugin" => Null,'prefix'=>'admin']) ?>
              </li>
			  <li>
			
				<?= $this->Html->link(__('Role Listing'), ['controller'=>'roles','action' => 'listing',"plugin" => Null,'prefix'=>'admin']) ?>
              </li>
              <li>
			
				<?= $this->Html->link(__('ACL'), ['controller'=>'Acl','action' => 'index',"plugin" => 'AclManager','prefix'=>FALSE]) ?>
              </li>
              
            </ul>
          </li>
          <!--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
            <a class="nav-link" href="#">
              <i class="fa fa-fw fa-link"></i>
              <span class="nav-link-text">
                Link</span>
            </a>
          </li>-->
        </ul>
        <ul class="navbar-nav sidenav-toggler">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle mr-lg-2" href="#" id="alertsDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-bell"></i>
              <span class="d-lg-none">Alerts
                <span class="badge badge-pill badge-warning">6 New</span>
              </span>
              <span class="new-indicator text-warning d-none d-lg-block">
                <i class="fa fa-fw fa-circle"></i>
                <span class="number">6</span>
              </span>
            </a>
            <div class="dropdown-menu" aria-labelledby="alertsDropdown">
              <h6 class="dropdown-header">New Alerts:</h6>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <span class="text-success">
                  <strong>
                    <i class="fa fa-long-arrow-up"></i>
                    Status Update</strong>
                </span>
                <span class="small float-right text-muted">11:21 AM</span>
                <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <span class="text-danger">
                  <strong>
                    <i class="fa fa-long-arrow-down"></i>
                    Status Update</strong>
                </span>
                <span class="small float-right text-muted">11:21 AM</span>
                <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <span class="text-success">
                  <strong>
                    <i class="fa fa-long-arrow-up"></i>
                    Status Update</strong>
                </span>
                <span class="small float-right text-muted">11:21 AM</span>
                <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item small" href="#">
                View all alerts
              </a>
            </div>
          </li>
          <li class="nav-item">
          <?php echo $this->Html->link('<i class="fa fa-fw fa-sign-out"></i>Logout',array('controller'=>'users','action'=>'logout',"plugin" => Null),array('class' => 'nav-link','escape' => false)); ?>
          </li>
        </ul>
      </div>
    </nav>
<!-- Bootstrap core JavaScript -->
<?php echo $this->Html->script('vendor/jquery/jquery.min.js'); ?>
<?php echo $this->Html->script('vendor/jquery/jquery.validate.min.js'); ?>
<?php echo $this->Html->script('dropzone.js'); ?>
<script type="text/javascript">Dropzone.autoDiscover = false;</script>
<?php echo $this->Html->script('custom1502.js'); ?>
<?php echo $this->Html->script('vendor/popper/popper.min.js'); ?>
<?php echo $this->Html->script('vendor/bootstrap/js/bootstrap.min.js'); ?>

<!-- Plugin JavaScript -->
<?php echo $this->Html->script('vendor/jquery-easing/jquery.easing.min.js'); ?>
<?php echo $this->Html->script('vendor/chart.js/Chart.min.js'); ?>
<?php echo $this->Html->script('vendor/datatables/jquery.dataTables.js'); ?>
<?php echo $this->Html->script('vendor/datatables/dataTables.bootstrap4.js'); ?>

<!-- Custom scripts for this template -->
<?php echo $this->Html->script('sb-admin.min.js'); ?>
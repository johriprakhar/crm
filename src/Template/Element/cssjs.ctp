<!-- sb admin-->
<!-- Bootstrap core CSS -->
<?= $this->Html->css('vendor/bootstrap/css/bootstrap.min.css') ?>
<!-- Custom fonts for this template -->
<?= $this->Html->css('vendor/font-awesome/css/font-awesome.min.css') ?>

<!-- Plugin CSS -->
<?= $this->Html->css('vendor/datatables/dataTables.bootstrap4.css') ?>

<!-- Custom styles for this template -->
<?= $this->Html->css('sb-admin.css') ?>
<?= $this->Html->css('dropzone.css') ?>
<?= $this->Html->css('custom.css') ?>
<?= $this->Html->css('default.css') ?>
<?= $this->fetch('meta') ?>
<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{   
    public $paginate = [
        // Other keys here.
        'limit' => 20
    ];
    public $helpers = [
        'Paginator' => ['templates' => 'paginator-templates']
    ];
    //allowed action without login
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
		$this->viewBuilder()->setTheme('Rokcrm');
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout']);
    }

    //login
    public function login()
    {
		$this->viewBuilder()->setTheme('Rok');
		//$this->layout = 'login';
        $this->viewBuilder()->setLayout('login');
        if ($this->Auth->user()) {
            return $this->redirect(['action' => 'index']);
        }
        //echo (new DefaultPasswordHasher)->hash('admin123');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
			
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    //logout
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function index()
    {
        
    }

    //user lsiting
    public function listing()
    {
        $this->paginate = [
        'contain' => ['Roles','Groups']
        ];
		$users = $this->paginate($this->Users);
	
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
		
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
        return $this->redirect(['action' => 'edit',$id]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		
		//$this->layout = 'login';
       // $this->viewBuilder()->setLayout('login');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'listing']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
		$this->loadModel('Roles');
		$this->loadModel('Groups');
        $this->set(compact('user'));
		$roles = $this->Roles->find('list');
		$this->set('roles', $roles);
		$groups = $this->Groups->find('list');
		$this->set('groups', $groups);
		//print_r($roles);
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'listing']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
		
		$this->loadModel('Roles');
		$this->loadModel('Groups');
        $this->set(compact('user'));
		$roles = $this->Roles->find('list',array('conditions'=>array('group_id'=>$user->group_id)));
		$this->set('roles', $roles);
		$groups = $this->Groups->find('list');
		$this->set('groups', $groups);
		
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	
	/**
     * Get Role method
     *
     * @param string|null $group_id Group id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	private function getRole($group_id = null)
    {
		$this->loadModel('Roles');
		$roles = $this->Roles->find('list',array('conditions'=>array('group_id'=>$group_id)));
		$roles_str = '';
		if(!empty($roles))
		{
			foreach($roles as $k=>$v)
			{
				$roles_str = "<option value=".$k.">".$v."</option>";
			}
		}
        return $roles_str;
    }
	
	/**
     * Ajax Role By Group method
     *
     * @param string|null .
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	public function ajaxRoleByGroup()
    {
        if ($this->request->is(['ajax', 'post'])) 
        {
            $id = $this->request->query('id');
            $data = ['data' => ['roles' => $this->getRole($id)]];
			$resultJ = json_encode($data);
			$this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
            //return json_encode($data);
        }
    }
}

<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class RolesController extends AppController
{   
    public $paginate = [
        // Other keys here.
        'limit' => 20
    ];
    public $helpers = [
        'Paginator' => ['templates' => 'paginator-templates']
    ];
    //allowed action without login
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout']);
		$this->viewBuilder()->setTheme('Rokcrm');
    }

    

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function index()
    {
        
    }

    //user lsiting
    public function listing()
    {
         $this->paginate = [
        'contain' => ['Groups']
        ];
		$roles = $this->paginate($this->Roles);
	
        $this->set(compact('roles'));
        $this->set('_serialize', ['roles']);
    }
   
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {
            $role = $this->Roles->patchEntity($role, $this->request->getData());
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'listing']);
            }
            $this->Flash->error(__('The role could not be saved. Please, try again.'));
        }
        $this->set(compact('role'));
		$this->loadModel('Groups');
		$groups = $this->Groups->find('list');
		$this->set('groups', $groups);
		//print_r($roles);
        $this->set('_serialize', ['role']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $role = $this->Roles->patchEntity($role, $this->request->getData());
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'listing']);
            }
            $this->Flash->error(__('The role could not be saved. Please, try again.'));
        }
	
		$this->loadModel('Groups');
		$groups = $this->Groups->find('list');
		$this->set('groups', $groups);
        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $role = $this->Roles->get($id);
        if ($this->Roles->delete($role)) {
            $this->Flash->success(__('The role has been deleted.'));
        } else {
            $this->Flash->error(__('The role could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'listing']);
    }
	
}

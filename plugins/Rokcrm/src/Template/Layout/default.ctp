<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html lang="en">
   <meta charset="utf-8" />
   <title>ROK-hardwar</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="" name="author" />
   <link href="https://fonts.googleapis.com/css?family=Muli:400,800" rel="stylesheet">
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link rel="shortcut icon" href="favicon.ico" />
   <script src="js/jquery.min.js" type="text/javascript"></script>
   <script src="js/bootstrap.min.js" type="text/javascript"></script>
		<!-- bootstrap & fontawesome -->
		<?= $this->Html->css('bootstrap.min.css') ?>
		<?= $this->Html->css('custom.css') ?>
		<?= $this->Html->script('jquery.min.js') ?>
		<?= $this->Html->script('bootstrap.min.js') ?>
		<?= $this->fetch('script') ?>
		
		<?= $this->fetch('css') ?>

		
	</head>

	   <body>
		<?php echo $this->element('header');?>
      <div class="clearfix"></div>
	  <?php echo $this->element('left-sidebar');?>
       
	  <div class="right-panel">
         <?= $this->fetch('content') ?>
      </div>
	  <script>
	  /*$(document).ready(function(){
		 var height = $('.right-panel').height()+30;
		 $('.left-panel').css('height',height);
	  })*/
	  </script>
   </body>
</html>
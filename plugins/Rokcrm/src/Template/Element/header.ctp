      <div class="header">
         <div class="logo">
            <img src="<?php echo $this->Url->image('logo.jpg');?>" class="img-responsive">
         </div>
         <div class="rightSide">
            <ul>
               <li>
                  <input type="text" placeholder="SEARCH" class="form-control search">
                  <span class="searchicon"><i class="fa fa-search" aria-hidden="true"></i></span>
               </li>
               <li>
               <li class="dropdown iconUp">
                  <a href="javascript:void(0)" class="uName" data-toggle="dropdown"> 
                  <span class="icon-user">
                  <img src="<?php echo $this->Url->image('user-icon.jpg');?>">
                  </span>									  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="#">My Profile</a></li>
                     <li><a href="#">My Item</a></li>
                     
                     
                  </ul>
               </li>
               </li>
               <li>
			    <?php echo $this->Html->link('<i class="fa fa-sign-in" aria-hidden="true"></i>Logout',array('controller'=>'users','action'=>'logout',"plugin" => Null,'prefix'=>'admin'),array('class' => 'nav-link','escape' => false)); ?>
                  
               </li>
            </ul>
         </div>
         <div class="clearfix"></div>
      </div>
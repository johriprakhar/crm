<div class="left-panel">
         <ul class="menu">
            <li>
               <a href="#">
               <i class="fa fa-plus-circle" aria-hidden="true"></i> Add New
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-pie-chart" aria-hidden="true"></i> Dashboard
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-user-plus" aria-hidden="true"></i> Leads
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-truck" aria-hidden="true"></i> Jobs
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-usd" aria-hidden="true"></i> Invoices
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-address-book-o" aria-hidden="true"></i> Contacts
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-calendar" aria-hidden="true"></i> Calendar
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-map" aria-hidden="true"></i> Load Map 
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-map-marker" aria-hidden="true"></i> Dispatch
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-folder" aria-hidden="true"></i> Docs
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-area-chart" aria-hidden="true"></i> Reports
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-sticky-note" aria-hidden="true"></i> Quickbooks
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-credit-card-alt" aria-hidden="true"></i> Online Pay
               </a>
            </li>
            <li>
               <a href="#">
               <i class="fa fa-dropbox" aria-hidden="true"></i> Dropbox
               </a>
            </li>
			<li>
				
               <?php echo $this->Html->link('<i class="fa fa-user-plus" aria-hidden="true"></i>Manage User',array('controller'=>'users','action'=>'listing',"plugin" => Null,'prefix'=>'admin'),array('class' => 'nav-link','escape' => false)); ?>
            </li>
			<li>
               <?php echo $this->Html->link('<i class="fa fa-user-plus" aria-hidden="true"></i>Manage Group',array('controller'=>'groups','action'=>'listing',"plugin" => Null,'prefix'=>'admin'),array('class' => 'nav-link','escape' => false)); ?>
              
            </li>
			<li>
               <?php echo $this->Html->link('<i class="fa fa-user-plus" aria-hidden="true"></i>Manage Roles',array('controller'=>'roles','action'=>'listing',"plugin" => Null,'prefix'=>'admin'),array('class' => 'nav-link','escape' => false)); ?>
            </li>
         </ul>
      </div>
     
<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create() ?>
    <div class="col-md-10 col-xs-12">
	<h1><?= __('Edit Group') ?></h1>
    <div class="card-body">
      <?= $this->Form->create($group) ?>
        
        <label>Group Name</label>
            
                <?= $this->Form->control('name',['label' =>false,'class' => 'form-control','placeholder' => 'Enter Group Name']) ?>

       
            <label>&nbsp;</label>
            
                <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
      </form>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>

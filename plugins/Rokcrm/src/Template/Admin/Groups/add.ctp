<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create($group,['id'=>'user_add']) ?>
  <div class="col-md-10 col-xs-12">
  <h1><?= __('Add Group') ?></h1>
    <div class="card-body">
        
        <label>Group Name</label>
                <?= $this->Form->control('name',['label' =>false,'class' => 'form-control required','placeholder' => 'Enter Group Name']) ?>
            <label>&nbsp;</label>
            
        <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
      </form>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>
<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create($role,['id'=>'user_add']) ?>
    <div class="col-md-10 col-xs-12">
	<h1><?= __('Add Role') ?></h1>
        <div class="card-body">
        <label>Role Name</label>
            
                <?= $this->Form->control('name',['label' =>false,'class' => 'form-control required','placeholder' => 'Enter Role Name']) ?>
		
            <label>Group</label>
                <?php 
				 $request = $this->Url->build(['action' => 'ajaxRoleByGroup', 'ext' => 'json']);
				echo $this->Form->control('group_id',['id' => 'groupField', 'rel' => $request,'class'=>'form-control required','options' => $groups,'label'=>false,'empty'=>'Select Group']);?>
           
       
            <label>&nbsp;</label>
            
                <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
           
        </div> 
		</div>
  <?= $this->Form->end() ?>
</div>
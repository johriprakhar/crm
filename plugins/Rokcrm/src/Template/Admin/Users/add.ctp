<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create($user,['id'=>'user_add']) ?>
  <div class="col-md-10 col-xs-12">
  <h1><?= __('Add User') ?></h1>
  
    <div class="card-body">
     <label>User Name</label>
            
                <?= $this->Form->control('username',['label' =>false,'class' => 'form-control required','placeholder' => 'Enter username']) ?>
    <div class="clearfix"></div>
      
            <label>Password</label>
            
                <?= $this->Form->control('password',['label' =>false,'type' => 'password','class' => 'form-control required','placeholder' => 'Password']) ?>   
				 <div class="clearfix"></div>
            <label>Email</label>
                            <?= $this->Form->control('email',['label' =>false,'type' => 'email','class' => 'form-control required','placeholder' => 'Email']) ?>
             <div class="clearfix"></div>
		            <label>Group</label>
                            <?php 
				 $request = $this->Url->build(['action' => 'ajaxRoleByGroup', 'ext' => 'json']);
				echo $this->Form->control('group_id',['id' => 'groupField', 'rel' => $request,'class'=>'form-control required','options' => $groups,'label'=>false,'empty'=>'Select Group']);?>
            <div class="clearfix"></div>
            <label>Role</label>
            
                <?php echo $this->Form->control('role_id',['id' => 'roleField','class'=>'form-control required','options' => array(),'label'=>false]);?>
             <div class="clearfix"></div>
            <label>&nbsp;</label>
            
                <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
      </form>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>
<script>
$(document).ready(function() {
    $('#groupField').change(function() 
    {
        var targeturl = $(this).attr('rel');
        var group_id = $(this).val();
        $.ajax({
            type: 'get',
            url: targeturl + '&id=' + group_id,
            beforeSend: function(xhr) 
            {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) 
            {
                if (response.data) 
                {
                    $('#roleField').html(response.data.roles);
                }
            },
            error: function(e) 
            {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    });
});
</script>
<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<?= $this->Flash->render() ?>
<?= $this->Form->create() ?>
  <div class="col-md-10 col-xs-12">
  <h1><?= __('Edit User') ?></h1>
    <div class="card-body">
      <?= $this->Form->create($user) ?>
       
        <label>User Name</label>
                <?= $this->Form->control('username',['label' =>false,'class' => 'form-control','placeholder' => 'Enter username']) ?>

            <label>Password</label>
                <?= $this->Form->control('password',['label' =>false,'type' => 'password','class' => 'form-control','placeholder' => 'Password']) ?>   
           
            <label>Email</label>
           
                <?= $this->Form->control('email',['label' =>false,'type' => 'email','class' => 'form-control ','placeholder' => 'Email']) ?>
            
            <label>Group</label>
            
                <?php 
				 $request = $this->Url->build(['action' => 'ajaxRoleByGroup', 'ext' => 'json']);
				echo $this->Form->control('group_id',['id' => 'groupField', 'rel' => $request,'class'=>'form-control required','options' => $groups,'label'=>false,'empty'=>'Select Group']);?>
           
			<label>Role</label>
	
				<?php echo $this->Form->control('role_id',['id' => 'roleField','class'=>'form-control required','options' => $roles,'label'=>false]);?>
	   
        
            <label>&nbsp;</label>
            
                <?= $this->Form->button('Submit',['class' => 'btn btn-primary btn-block col-sm-4']); ?>
            
      </form>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>
<script>
$(document).ready(function() {
    $('#groupField').change(function() 
    {
        var targeturl = $(this).attr('rel');
        var group_id = $(this).val();
        $.ajax({
            type: 'get',
            url: targeturl + '&id=' + group_id,
            beforeSend: function(xhr) 
            {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) 
            {
                if (response.data) 
                {
                    $('#roleField').html(response.data.roles);
                }
            },
            error: function(e) 
            {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    });
});
</script>
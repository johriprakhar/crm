$(document).ready(function(){
    var base_url = base_url();
    // get base url
    function base_url() {
        var pathparts = location.pathname.split('/');
        if (location.host == 'localhost') {
            var url = location.origin+'/'+pathparts[1].trim('/')+'/'; // http://localhost/myproject/
        }else{
            var url = location.origin; // http://stackoverflow.com
        }
        return url;
    }
    //form validations
    $("#user_login").validate({
        rules: {
                    input: { required: true},
                }
    });
    $("#user_add").validate({
        rules: {
                    input: { 
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    email: {
                        required:true,
                        email: true
                    }
                },
        messages: {
                    password: {
                        required: "Please enter password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: "Please enter a valid email address"
                }

    });
    // report upload form
    /*$('#uploadreport').submit(function(e){
        e.preventDefault();
        $('.overlay').show();
        form_url = base_url+$(this).attr('action');
        var formdatas = new FormData($('#uploadreport')[0]);
        $.ajax({
            url: form_url,
            dataType: 'json',
            method: 'post',
            data:  formdatas,
            contentType: false,
            processData: false
        })
            .done(function(response) {
                console.log(response);
                //show result
                if (response.sellingCoachReport == 1) {
                    location.reload();
                } else if (response.sellingCoachReport == 0) {
                    location.reload();
                } else {
                    location.reload();
                }
                $('.overlay').hide();
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 403) {
                    window.location = '/';
                } else {
                    console.log(jqXHR);

                }
                $('.overlay').hide();
            });

    });*/
    form_url = base_url+$('#my-awesome-dropzone').attr('action');
    $("#my-awesome-dropzone").dropzone({
        maxFiles: 1,
        url: form_url,
        addedfile: function(file) {
            $('.overlay').show();
        },
        success: function(file, response){
            if (response.sellingCoachReport == 1) {
                location.reload();
            } else if (response.sellingCoachReport == 0) {
                location.reload();
            } else {
                location.reload();
            }
            $('.overlay').hide();        
        }
    });

    $('.inventory_link').click(function(){
        $('.overlay').show(); 
    });

    /*$("#kitskuform").submit(function( event ) {
        $('.overlay').show();
    });*/
    
});
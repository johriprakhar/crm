<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SellingCoachReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SellingCoachReportsTable Test Case
 */
class SellingCoachReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SellingCoachReportsTable
     */
    public $SellingCoachReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.selling_coach_reports'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SellingCoachReports') ? [] : ['className' => SellingCoachReportsTable::class];
        $this->SellingCoachReports = TableRegistry::get('SellingCoachReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SellingCoachReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FbaReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FbaReportsTable Test Case
 */
class FbaReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FbaReportsTable
     */
    public $FbaReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fba_reports'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FbaReports') ? [] : ['className' => FbaReportsTable::class];
        $this->FbaReports = TableRegistry::get('FbaReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FbaReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

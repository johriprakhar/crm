<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SkuVaultReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SkuVaultReportsTable Test Case
 */
class SkuVaultReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SkuVaultReportsTable
     */
    public $SkuVaultReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sku_vault_reports'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SkuVaultReports') ? [] : ['className' => SkuVaultReportsTable::class];
        $this->SkuVaultReports = TableRegistry::get('SkuVaultReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SkuVaultReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
